# Image classification of clothing patterns

The task goal was pretty straight forward and simple. Or so I thought. [Read more](#the-build)


## Steps to run

```bash
# Download latest release from the following link:
# https://gitlab.com/arjun921/task/releases
# Go get a cup of coffee, the download might take a while depending on your internet speed. (260mb~)
# Unzip task-*
$ cd task-*
$ mkdir uploads
$ pipenv install --skip-lock
# if pipenv not found, install using `pip3 install pipenv`
$ pipenv shell
$ python3 run.py
# open localhost:5000 and upload a file/submit a url.
```

## Summary

- The application supports `\n` seperated file urls for classification, as well as direct file upload for files on the local machine. Screenshots attached in the next section.
- Deep Fashion dataset was used to improve the model Accuracy. - [Link](http://mmlab.ie.cuhk.edu.hk/projects/DeepFashion/AttributePrediction.html)
- Model used was InceptionV3, a state-of-the-art convolutional deep neural network (for 2015) and it was partially trained by transfer learning.
- The final model has a final test accuracy of 86.3%.
- Output for test folder: [submission.csv](./submission.csv).

For more detailed information: [README](./README.md)

## API

There are two REST API endpoints defined as follows:

1. `/inference_files`
   - Method: `POST`
   - Request type: `form-data`
   - Encoding Type: `multipart/form-data`
   - Expected Input:
     - Single/Multiple Files
     - Form field name: `file[]`
   - Output:
     - Array of JSONs with file name and top 3 predicted classes and their confidence scores.
     ```json
     [{'66972.jpg': [{'patterned': '0.78545016'}, {'printed': '0.04683444'}, {'floral': '0.040152982'}]}, 
     {'66981.jpg': [{'patterned': '0.49286833'}, {'melange': '0.37304768'}, {'printed': '0.07632087'}]} ,
     {'66970.jpg': [{'patterned': '0.5515923'}, {'checked': '0.2170392'}, {'printed': '0.17263392'}]} ,
     {'66971.jpg': [{'solid': '0.48433498'}, {'patterned': '0.33287543'}, {'melange': '0.08782025'}]} ,
     {'66973.jpg': [{'patterned': '0.7041942'}, {'melange': '0.15320641'}, {'printed': '0.08262117'}]} ,
     {'67849.jpg': [{'colourblock': '0.5435568'}, {'abstract': '0.41794'}, {'graphic': '0.01130499'}]} ]
     ```

2. `/inference_urls`
   - Method: `POST`
   - Request type: `form-data`
   - Encoding Type: `application/x-www-form-urlencoded`
     - Expected Input:
        - Publicly accessible URLs to images seperated by `\n`
        - Form field name: `urls_txt`
     - Output:
       - Array of JSONs with file name and top 3 predicted classes and their confidence scores.
     ```json
     [{'66972.jpg': [{'patterned': '0.78545016'}, {'printed': '0.04683444'}, {'floral': '0.040152982'}]}, 
     {'66981.jpg': [{'patterned': '0.49286833'}, {'melange': '0.37304768'}, {'printed': '0.07632087'}]} ,
     {'66970.jpg': [{'patterned': '0.5515923'}, {'checked': '0.2170392'}, {'printed': '0.17263392'}]} ,
     {'66971.jpg': [{'solid': '0.48433498'}, {'patterned': '0.33287543'}, {'melange': '0.08782025'}]} ,
     {'66973.jpg': [{'patterned': '0.7041942'}, {'melange': '0.15320641'}, {'printed': '0.08262117'}]} ,
     {'67849.jpg': [{'colourblock': '0.5435568'}, {'abstract': '0.41794'}, {'graphic': '0.01130499'}]} ]
     ```

# The Build.

Initial analysis of the dataset revealed multiple inaccurate images under the wrong `label` folder. On realizing data cleaning would take a while, I decided to implement everything else first, and decided to just swap out the trained model afterwards. 

The classifer was built in the following order of steps:

1. Data Exploration - Realized there were wrong labels for quite a lot of classes.

2. Dry run image classification using broilerplate InceptionV3 code - I had done image classification a lot of times, so I had the code to train/predict on the images. Just had to point to the train directory and I had a trained model(with very poor accuracy) ready.

3. Setup an Web API with frontend - Again, used my broilerplate code for quick up-and-running with Flask. 

   1. Implemented file upload and passed uploaded files through the classifer(InceptionV3)
   2. Implemented file downloader for sending images as a URL directly to the classifier for inference.
      - At this point I had a base submission ready, but I still had to improve on the model test accuracy.

4. Data Cleaning - Traversing each class, deleting irrelevant images.  [Read more](#data-cleaningimproving-accuracy)

5. Retraining -  `Final test accuracy = 71.5% (N=302)  `

   ![](https://i.postimg.cc/C1n4DZxH/Screenshot-2019-04-05-at-7-55-59-PM.png)

6. Deleting more `false positives` in the training set, that I missed out in the intitial data cleaning, adding replacement images.

7. Retraining again - `Final test accuracy = 85% (N=254)  `

   ![](https://i.postimg.cc/Gp5QHPdm/Screenshot-2019-04-05-at-7-56-31-PM.png)

8. Reviewing misclassified images (again)

9. Deleting `false positive` images from classes I didn't delete in step 6

10. The Final Training - `Final test accuracy = 86.1% (N=245)  `

    ![](https://i.postimg.cc/ZqRHbskQ/Screenshot-2019-04-05-at-8-40-13-PM.png)

11. Reviewing misclassified images (One Last Time)

12. Deleting `false positive` images from classes I didn't delete in step 9, this time adding replacement images for few classes with a really bad data set.

13. The Final Training - `Final test accuracy = 86.3% (N=249)  `

![](https://i.postimg.cc/mZ05d4xg/Screenshot-2019-04-05-at-9-57-54-PM.png)

## Data Cleaning/Improving Accuracy

Building the backend/frontend and integrating the classifier with the backend  done in about 6 hours. 

The the next `6 hours` were spent painstalkingly `traversing through all the classes of images` to delete (many) images from multiple classes to ensure, the most prestine dataset was being fed to the the classifier during training. 

> It's like one red sock can ruin the whole laundry of whites and make them all pink.

Same holds true for ML as well. 

Since so many images had to be deleted, we had to replace it with new images as well. Instead of scraping the web for images, I found a dataset called [deep fashion](http://mmlab.ie.cuhk.edu.hk/projects/DeepFashion/AttributePrediction.html).  This was a huge collection and I found every class I needed.

To ensure the right images were going in to the training set, I hand picked images to fill the void from deleting the irrelvant images from the originally provided dataset.

## Further Improvements

For further improvements, we could consider taking the following steps:
 - Using a capsule net which makes the model robust to the pixel attack. 
 - Handpicking images for adding more classes.
 - Training the model on images of patterns[(example)](https://www.google.com/search?q=stripes&safe=active&client=safari&rls=en&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiqo-uJr7rhAhVlhuAKHZ7yAL0Q_AUIDigB&biw=1280&bih=625) instead of images of dresses could improve the model significantly (needs testing to validate hypothesis).


### Screenshots

![https://i.postimg.cc/zvhZdSqd/Screenshot-2019-04-06-at-12-25-10-AM.png](https://i.postimg.cc/zvhZdSqd/Screenshot-2019-04-06-at-12-25-10-AM.png)

![https://i.postimg.cc/jSbKF9wy/Screenshot-2019-04-06-at-12-25-32-AM.png](https://i.postimg.cc/jSbKF9wy/Screenshot-2019-04-06-at-12-25-32-AM.png)