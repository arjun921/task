# config.py

# Enable Flask's debugging features. Should be False in production
DEBUG = False
HOST='0.0.0.0'
SAVED_MODEL='trained_model/unoptimized_model.pb'
SAVED_LABELS='trained_model/labels.txt'
UPLOAD_FOLDER='uploads/'