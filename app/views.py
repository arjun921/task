from flask import Flask, request, render_template, current_app,redirect,url_for
from werkzeug.utils import secure_filename
import tensorflow as tf
import requests
import json
import os
from app import app

graph_file = os.getenv('SAVED_MODEL') or 'trained_model/unoptimized_model.pb'
labels_txt = os.getenv('SAVED_LABELS') or 'trained_model/labels.txt'

with tf.gfile.FastGFile(graph_file,'rb') as f:
	graph_def = tf.GraphDef()
	graph_def.ParseFromString(f.read())
	_ = tf.import_graph_def(graph_def,name='')

label_lines = [line.rstrip() for line in tf.gfile.GFile(labels_txt)]
sess = tf.Session()


# Other functions
def classify(filenames):
    """Inference using protobuf frozen graph"""
    images = []
    for image_path in filenames:
        image_predictions = {}
        image_data = tf.gfile.FastGFile(app.config['UPLOAD_FOLDER']+'/'+image_path,'rb').read()
        softmax_tensor = sess.graph.get_tensor_by_name('final_result:0')
        predictions = sess.run(softmax_tensor,{'DecodeJpeg/contents:0':image_data})
        top_k = predictions[0].argsort()[-len(predictions[0]):][::-1]
        labels_for_image = []
        for node_id in top_k[:3]:
            # just give top 3 predictions
            category = {}
            human_string = label_lines[node_id]
            score = predictions[0][node_id]
            # print('%s (score = %.5f)' % (human_string, score))
            category[human_string] = str(score)
            labels_for_image.append(category)
        image_predictions[image_path] = labels_for_image
        images.append(image_predictions)
    # print(images)
    return images



# Web page renders
@app.route('/home')
def home():
    return render_template("index.html",result_json=request.args.get('result_json'))

@app.route('/')
def index():
    return render_template("index.html",result_json=request.args.get('result_json'))


# Webservices
@app.route('/inference_files', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        uploaded_files = request.files.getlist("file[]")
        filenames = []
        for file in uploaded_files:
            # Make the filename safe, remove unsupported chars
            filename = secure_filename(file.filename)
            # Move the file form the temporal folder to the upload
            # folder we setup
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            # Save the filename into a list, we'll use it later
            filenames.append(filename)

        # run classifier
        predictions = classify(filenames)
        # response = {'predictions':predictions}
    return render_template("index.html",result_json=predictions)

@app.route('/inference_urls', methods=['GET', 'POST'])
def inference_urls():
    if request.method == 'POST':
        img_urls = request.form['urls_txt'].strip().split('\r\n')
        filenames = []
        for url in img_urls:
            r = requests.get(url)
            filename = url.split("/")[-1] 
            with open(app.config['UPLOAD_FOLDER']+'/'+ filename, 'wb') as f:  
                f.write(r.content)        
            filenames.append(filename)

        # run classifier
        predictions = classify(filenames)
        # response = {'predictions':predictions}
        return render_template("index.html",result_json=predictions)